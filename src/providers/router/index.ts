import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../../views/HomeView.vue'
import { initLogger, writeLogs } from '@/providers/router/logger'
import AboutView from '@/views/AboutView.vue'
import DocumentationView from '@/views/DocumentationView.vue'
import ToolingView from '@/views/ToolingView.vue'

export const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
      component: AboutView
    },
    {
      path: '/documentation',
      name: 'documentation',
      component: DocumentationView
    },
    {
      path: '/tooling',
      name: 'tooling',
      component: ToolingView
    }
  ]
});

initLogger();
router.beforeEach((to) => writeLogs(to));
