import { type RouteLocationNormalized } from 'vue-router';
import { ref } from 'vue'

let logs: any = {};

export const customerId = ref(Math.floor(Math.random() * 10).toString());
const yesterdayDate = new Date(Date.now() - 864e5).toLocaleDateString();
const todayDate = new Date().toLocaleDateString();

export const initLogger = () => {
  // add logs for yesterday visits
  const initialState = {
    '0' : new Set(['home', 'about']),
    '1' : new Set(['home']),
    '2' : new Set(['home']),
    '3' : new Set(['home']),
    '4' : new Set(['home']),
    '5' : new Set(['home']),
    '6' : new Set(['home']),
    '7' : new Set(['home']),
    '8' : new Set(['home']),
    '9' : new Set(['home']),
    '10': new Set(['home']),
    'twoDaysCustomers': new Set([])
  }
  logs = {
    [yesterdayDate]: initialState,
    [todayDate]: {}
  }
}

export const writeLogs = (
  to: RouteLocationNormalized,
) => {
  // add a line to today log
  if (customerId.value in logs[todayDate]) {
    logs[todayDate][customerId.value].add(to.name);
  } else {
    logs[todayDate][customerId.value] = new Set([to.name]);
  }

  // go on with yesterday log, concat it with new today visits
  if (customerId.value in logs[yesterdayDate]) {
    logs[yesterdayDate][customerId.value].add(to.name);
    logs[yesterdayDate].twoDaysCustomers.add(customerId.value);
  }
};

export const printLeads = () => {
  if (!(yesterdayDate in logs)) {
    throw new Error('No logs for previous day');
  }

  // build a final list of users
  const listOfUsers: Array<string> = [];
  for (const customer in logs[yesterdayDate]) {
    if (
      logs[yesterdayDate].twoDaysCustomers.has(customer) &&
      logs[yesterdayDate][customer].size >= 2) {
      listOfUsers.push(customer);
    }
  }
  console.log('list of visits: ', logs);
  console.log('list of users by customerId: ', listOfUsers);
  return listOfUsers;
}


